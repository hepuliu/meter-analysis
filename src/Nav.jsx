import React, {Component} from 'react';

class Nav extends Component {
    render() {
        return (
            <nav className="navbar">
                <h1>Water Smart</h1>
            </nav>
        );
    }
}

export default Nav;
